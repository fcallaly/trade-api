def projectName = 'demo-trade-api'
def version = "0.0.${currentBuild.number}"
def dockerImageTag = "${projectName}:${version}"
//def dockerRepository = "fcallaly.jfrog.io/default-docker-local"

pipeline {
  agent any
 
  stages {
    stage('Test') {
      steps {
        sh 'chmod a+x gradlew'
        sh './gradlew test'
      }
    }

    stage('Build') {
      steps {
        sh './gradlew build'
      }
    }

    stage('Build Container') {
      steps {
        sh "docker build -t ${dockerImageTag} ."
      }
    }

    stage('Deploy Container To Openshift') {
      environment {
        OPENSHIFT_CREDS = credentials('openshiftCreds')
      }
      steps {
        // login and either use and existing project OR create a new project
        sh "oc login -u ${OPENSHIFT_CREDS_USR} -p ${OPENSHIFT_CREDS_PSW}"
        sh "oc project ${projectName} || oc new-project ${projectName}"
        
        // deploy a mongo container from dockerhub if one is not in this project alread
        sh "oc get service mongo || oc new-app mongo"
        
        // remove previous deployments of this app
        sh "oc delete all --selector app=${projectName} || echo 'Unable to delete all previous openshift resources'"
        
        // deploy the app and create an external route to it
        sh "oc new-app ${dockerImageTag} -l version=${version} -e DB_HOST=mongo"
        sh "oc expose svc/${projectName}"
      }
    }
  }

  post {
    always {
      archiveArtifacts artifacts: 'build/libs/**/*.jar', fingerprint: true
      archiveArtifacts artifacts: 'build/jacoco/**/*'
      archiveArtifacts 'build/reports/**/*'
    }
  }
}
