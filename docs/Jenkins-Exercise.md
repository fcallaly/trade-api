## Create a Jenkins Project to deploy this Pipeline

1. Login to jenkins e.g. http://reconnect-docker-50.conygre.com:8080
    - Jenkins is on port 8080 of your linux machine
    - username: admin, password: c0xxxx
    - (usual password)
   
2. Make a folder to put your project in:
    - New Item
    - <Enter Folder Name> e.g. Training Project
    - Click "Folder"
    - Click "Ok"
    - Click "Save"
   
3. Add a project item to your Jenkins Folder:
    - New Item
    - <Enter Item Name> e.g. Trade API
    - Click "Pipeline"
    - Click "OK"
   
4. You are now on the Item configuration screen.
    - Scroll down to the Pipeline section
    - In the dropdown select 'Pipeline script from SCM'
    - In the SCM dropdown select 'Git'
    - In Repository URL enter: ```https://bitbucket.org/fcallaly/trade-api.git```
    - Click "Save"

5. Your Jenkins project is created.
    - To trigger the Pipeline manually click ```Build Now```
    - This pipeline should build a container and deploy it, along with a mongoDB container, into your openshift
    - Verify that you have a project and containers in Openshift
    - Try to understand the contents of ```Jenkinsfile``` in this bitbucket repository
